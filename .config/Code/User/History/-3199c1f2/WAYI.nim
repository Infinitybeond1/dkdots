import os, osproc, strutils

let configDir: string = getConfigDir() & "lite-xl/"
let lspDir: string = getConfigDir() & "lite-xl/lualsp"

proc installLite(shellrc: string): int =
  execCmd("""wget https://github.com/lite-xl/lite-xl/releases/download/v2.0.5/lite-xl-luajit-linux-x86_64.tar.gz && 
   tar -xvf lite-xl-luajit-linux-x86_64.tar.gz &&
   mkdir -p $HOME/.local/bin &&
   cp bin/lite-xl $HOME/.local/bin && 
   cp -r share $HOME/.local && 
   echo -e 'export PATH=$PATH:$HOME/.local/bin' >> $HOME/.bashrc
  """)
  return 0

  
proc installChonk(): int =
  if dirExists(getConfigDir() & "lite-xl"):
    discard execCmd("""mv ~/.config/lite-xl ~/.config/lite-xl.bak && 
     git clone https://github.com/Chonk-xl/chonk-xl && 
     cd chonk-xl && 
     mv * ~/.config/lite-xl && 
     cd .. &&
     rm -r chonk-xl
    """)
    return 0
  else:
    discard execCmd("""git clone https://github.com/Chonk-xl/chonk-xl &&
     cd chonk-xl && 
     mv * ~/.config/lite-xl && 
     cd .. &&
     rm -r chonk-xl
    """)
    return 0

proc installLuaLsp(): int =
  
  
  execCmd("""
   
  """)


    
