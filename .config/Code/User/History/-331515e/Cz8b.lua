local lspconfig = require "plugins.lsp.config"

lspconfig.sumneko_lua.setup {
    command = {
    "~/.config/lite-xl/lualsp/lua-language-server",
    "-E",
    "~/.config/lite-xl/lualsp/main.lua"
  },
  settings = {
    Lua = {
      diagnostics = {
        enable = false
      }
    }
  }
}

lspconfig.nimlsp.setup()

