local lspconfig = require "plugins.lsp.config"

lspconfig.sumneko_lua.setup {
    command = {
    "~/.config/lite-xl/lua-lsp/lua-language-server",
    "-E",
    "~/.config/lite-xl/lua-lsp/main.lua"
  },
  settings = {
    Lua = {
      diagnostics = {
        enable = false
      }
    }
  }
}

lspconfig.nimlsp.setup()

